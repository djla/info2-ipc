/***** Include *****/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>
#include <errno.h>
#include <pthread.h>


/***** Tipo de datos *****/
typedef struct data_s
{
	pid_t producerPid;					// Pid del productor de datos
	unsigned int time;					// Cantidad de segundos en ejecucion
	bool ready;							// El productor pone a true cuando el dato esta listo, el consumidor pone a false cuando lo consumio
} Data_s;

/***** Variables globales privadas *****/
static volatile bool end = false;
static Data_s Data = {0};	// Variable que se utiliza para comunciar los threads.

static pthread_t consumerThreadId;					// Id del thread "consumer"
static pthread_t producerThreadId;					// Id del thread "producer"

/***** Prototipo de funciones privadas *****/
static void * ProduceData(void *arg);
static void * ConsumeData(void *arg);
static void Init(void);
static void SigIntHanler(int sig);
static void ExitOnError(bool condition, const char *str);

/***** Programa principal *****/
/*
 */
int main (void)
{
	int retVal;
	unsigned int sec;	// Sera pasado como argumento al thread producer

	Init();

	printf("Soy el Proceso Principal que creara dos threads, un productor y un consumidor de datos. Mi Pid es: %d\n",getpid());

	// Paso 1. Creo un nuevo thread para el consumidor
	retVal = pthread_create(&consumerThreadId, NULL, &ConsumeData, NULL);
	ExitOnError((retVal == -1), "Falló la función pthread_create()"); // Verifica el valor de retorno de pthread_create, en caso de error sale del programa

	// Paso 2. Creo un nuevo thread para el productor
    retVal = pthread_create(&producerThreadId, NULL , &ProduceData, (void *)&sec);
    ExitOnError((retVal == -1), "Falló la función pthread_create()"); // Verifica el valor de retorno de pthread_create, en caso de error sale del programa

    // Paso 3. El proceso principal, esperará a que todos los threads finalicen medinate pthread_join()
    retVal = pthread_join(consumerThreadId, NULL);
	ExitOnError((retVal == -1), "Falló la función pthread_join()"); // Verifica el valor de retorno de pthread_join, en caso de error sale del programa

	retVal = pthread_join(producerThreadId, NULL);
    ExitOnError((retVal == -1), "Falló la función pthread_join()"); // Verifica el valor de retorno de pthread_join, en caso de error sale del programa

	// Paso 4. Finalizo imprimiendo un cartel
	puts("Finaliza el Proceso Principal\n");

	return EXIT_SUCCESS;
}


/***** Implementacion de funciones *****/

static void * ProduceData(void *arg)
{
	unsigned int *sec = (unsigned int *)arg;

	while(end == false)	// Finaliza al presionar "Ctrl + C"
	{
		if (Data.ready == false)	// Si no hay datos, carga un nuevo dato
		{
			Data.producerPid = getpid();	// Carga el pid del proceso
			Data.time = *sec;				// Carga el tiempo de ejecucion
			Data.ready = true;				// Indica que hay un dato listo para ser consumido
		}

		(*sec)++;								// Actualiza los segundos en ejecucion
		sleep(1);
	}

	puts("Finaliza el Thread Productor\n");

	return NULL;
}

static void * ConsumeData(void *arg)
{
	while(end == false)	// Finaliza al presionar "Ctrl + C"
	{
		if (Data.ready == true)	// Verifica si hay un dato listo para ser consumido
		{
			printf("Pid: %d, Tiempo:%d\n", Data.producerPid, Data.time);

			Data.ready = false;		// Reestablece el flag para que luego puedan ser cargados mas datos-
		}
	}

	puts("Finaliza el Trhread Consumidor\n");

	return NULL;
}


/* Imprime un titulo y actualiza el handler para manejar SIGINT */
static void Init(void)
{
	puts("**********************\n***   THREADS      *** \n**********************\n");
	signal(SIGINT, SigIntHanler);	// Actualiza el handler para la "signal" SIGINT
}

/* Al presionar "Ctrl + C" finalizara el programa  */
static void SigIntHanler(int sig)
{
	(void)sig;
	end = true;
}

static void ExitOnError(bool condition, const char *str)
{
	if (condition == true)
	{
		perror(str);
		exit(EXIT_FAILURE);
	}
}

